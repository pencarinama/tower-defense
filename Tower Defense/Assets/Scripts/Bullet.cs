﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    //if enemy dead after shoot but before bullet hit, reset bullet delay
    private Tower _shootTower;
    private int _bulletPower;
    private float _bulletSpeed;
    private float _bulletSplashRadius;
    private bool _enemyIsHit;

    private Enemy _targetEnemy;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {

        if(LevelManager.Instance.isOver)
        {
            return;
        }

        if(_targetEnemy != null)
        {
            if(!_targetEnemy.gameObject.activeSelf)
            {
                gameObject.SetActive(false);
                _targetEnemy = null;

                if(!_enemyIsHit)
                {
                    _shootTower.ResetDelay();
                }
                return;
            }

            transform.position = Vector3.MoveTowards(transform.position, _targetEnemy.transform.position, _bulletSpeed * Time.fixedDeltaTime);
            Vector3 direction = _targetEnemy.transform.position - transform.position;

            float targetAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, targetAngle - 90f));
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(_targetEnemy == null)
        {
            return;
        }

        if(collision.gameObject.Equals(_targetEnemy.gameObject))
        {
            _enemyIsHit = true;
            gameObject.SetActive(false);

            if (_bulletSplashRadius > 0f)
            {
                LevelManager.Instance.ExplodeAt(transform.position, _bulletSplashRadius, _bulletPower);
            }
            else
            {
                _targetEnemy.ReduceEnemyHealth(_bulletPower);
            }
            _targetEnemy = null;
        }
    }


    public void SetProperties(int bulletPower, float bulletSpeed, float bulletSplashRadius, Tower tower)
    {
        _bulletPower = bulletPower;
        _bulletSpeed = bulletSpeed;
        _bulletSplashRadius = bulletSplashRadius;
        _shootTower = tower;
        _enemyIsHit = false;
    }

    public void SetTargetEnemy(Enemy enemy)
    {
        _targetEnemy = enemy;
    }
}
